<?php

namespace App\Models\Article;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'slug', 'body', 'subject_id'];

    public function user()
    {
        return $this->belongsTo(User::class); // 1 Artikel dimiliki 1 User
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class); // 1 Artikel dimiliki 1 Subject
    }

    // Kenapa kedua relasi tersebut menggunakan belongsTo bukan hasMany or HasOne?
    // Karena hasMany atau hasOne nya sudah disetting di model subject dan user (model yang bersangkutan) jadi cukup belongsTo di model ini
}
