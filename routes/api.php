<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\{RegisterController, LoginController, LogoutController};
use App\Http\Controllers\Article\ArticleController;
use App\Http\Controllers\UserController;

Route::post('register', RegisterController::class);
Route::post('login', LoginController::class);
Route::post('logout', LogoutController::class);

Route::group(['middleware' => 'api'], function () {
    Route::post('create-new-article', [ArticleController::class, 'store']);
    Route::get('user', UserController::class);
});





// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
